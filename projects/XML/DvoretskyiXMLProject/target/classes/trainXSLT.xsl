<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  version="1.0">
  <xsl:template match="/">
    <html>
      <head>
        <style type="text/css">
          table.tfmt {
          border: 2px ;
          }

          td.colfmt {
          border: 2px ;
          background-color: red;
          color: yellow;
          text-align:right;
          }

          th {
          background-color: #2E9AFE;
          color: white;
          }

        </style>
      </head>

      <body>
        <table class="tfmt">
          <tr>
            <th style="width:250px">Name</th>
            <th style="width:350px">type</th>

            <th style="width:250px">carriages</th>


          </tr>

          <xsl:for-each select="trains/train">

            <tr>
              <td class="colfmt">
                <xsl:value-of select="@trainNo"/>
              </td>

              <td class="colfmt">
                <xsl:value-of select="name"/>
              </td>
              <td class="colfmt">
                <xsl:value-of select="type"/>
              </td>


              <td class="colfmt">
                <xsl:value-of select="carriage"/>
              </td>


            </tr>

          </xsl:for-each>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>