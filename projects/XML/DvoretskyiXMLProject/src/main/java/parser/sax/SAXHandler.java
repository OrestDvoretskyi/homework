package parser.sax;


import java.util.ArrayList;
import java.util.List;
import model.Carriage;
import model.Train;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class SAXHandler extends DefaultHandler {

  private List<Train> trainList = new ArrayList<>();
  private Train train = null;
  private List<Carriage> carriages = null;

  private boolean bName = false;
  private boolean bType = false;
  private boolean bCarriages = false;
  private boolean bCarriage = false;


  public List<Train> getTrainList() {
    return this.trainList;
  }

  public void startElement(String uri, String localName, String qName, Attributes attributes)
      throws SAXException {
    if (qName.equalsIgnoreCase("train")) {
      String trainId = attributes.getValue("trainId");
      train = new Train();
      train.setId(Integer.parseInt(trainId));
    } else if (qName.equalsIgnoreCase("name")) {
      bName = true;
    } else if (qName.equalsIgnoreCase("type")) {
      bType = true;

    } else if (qName.equalsIgnoreCase("carriages")) {
      bCarriages = true;
    } else if (qName.equalsIgnoreCase("carriage")) {
      bCarriage = true;
    }
  }

  public void endElement(String uri, String localName, String qName) throws SAXException {
    if (qName.equalsIgnoreCase("train")) {
      trainList.add(train);
    }
  }

  public void characters(char ch[], int start, int length) throws SAXException {
    if (bName) {
      train.setName(new String(ch, start, length));
      bName = false;
    } else if (bType) {
      train.setType(new String(ch, start, length));
      bType = false;

    } else if (bCarriages) {
      carriages = new ArrayList<>();
      bCarriages = false;
    } else if (bCarriage) {
      Carriage carriage = new Carriage("Carriage");
      carriage.setIdNumber(Integer.parseInt(new String(ch, start, length)));
      carriages.add(carriage);
      bCarriage = false;


    }
  }
}

