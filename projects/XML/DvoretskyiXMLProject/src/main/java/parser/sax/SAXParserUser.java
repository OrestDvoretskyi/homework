package parser.sax;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import model.Train;
import org.xml.sax.SAXException;

public class SAXParserUser {

  private static SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();

  public static List<Train> parseTrains(File xml, File xsd) {
    List<Train> trainList = new ArrayList<>();
    try {
      saxParserFactory.setSchema(SAXValidator.createSchema(xsd));

      SAXParser saxParser = saxParserFactory.newSAXParser();
      SAXHandler saxHandler = new SAXHandler();
      saxParser.parse(xml, saxHandler);

      trainList = saxHandler.getTrainList();
    } catch (SAXException | ParserConfigurationException | IOException ex) {
      ex.printStackTrace();
    }

    return trainList;
  }
}
