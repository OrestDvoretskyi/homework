package parser.stax;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import model.Carriage;
import model.Train;

public class StAXReader {

  public static List<Train> parseTrains(File xml, File xsd) {
    List<Train> trainList = new ArrayList<>();
    Train train = null;
    //Chars chars = null;
    //SpillMethod spillMethod = null;
    List<Carriage> carriages = null;

    XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
    try {
      XMLEventReader xmlEventReader = xmlInputFactory
          .createXMLEventReader(new FileInputStream(xml));
      while (xmlEventReader.hasNext()) {
        XMLEvent xmlEvent = xmlEventReader.nextEvent();
        if (xmlEvent.isStartElement()) {
          StartElement startElement = xmlEvent.asStartElement();
          String name = startElement.getName().getLocalPart();
          switch (name) {
            case "train":
              train = new Train();

              Attribute idAttr = startElement.getAttributeByName(new QName("trainId"));
              if (idAttr != null) {
                train.setId(Integer.parseInt(idAttr.getValue()));
              }
              break;
            case "name":
              xmlEvent = xmlEventReader.nextEvent();
              assert train != null;
              train.setName(xmlEvent.asCharacters().getData());
              break;
            case "type":
              xmlEvent = xmlEventReader.nextEvent();
              assert train != null;
              train.setType(xmlEvent.asCharacters().getData());
              break;

            case "carriages":
              xmlEvent = xmlEventReader.nextEvent();
              carriages = new ArrayList<>();
              break;
           case "carriage":
              xmlEvent = xmlEventReader.nextEvent();
              assert carriages != null;
              carriages.add(new Carriage(xmlEvent.asCharacters().getData()));
              break;

                       }
        }

        if (xmlEvent.isEndElement()) {
          EndElement endElement = xmlEvent.asEndElement();
          if (endElement.getName().getLocalPart().equals("train")) {
            trainList.add(train);
          }
        }
      }

    } catch (FileNotFoundException | XMLStreamException e) {
      e.printStackTrace();
    }
    return trainList;
  }
}
