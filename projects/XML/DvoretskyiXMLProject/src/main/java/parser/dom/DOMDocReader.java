package parser.dom;

import java.util.ArrayList;
import java.util.List;
import model.Carriage;
import model.Train;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class DOMDocReader {

  public List<Train> readDoc(Document doc) {
    doc.getDocumentElement().normalize();
    List<Train> trains = new ArrayList<>();

    NodeList nodeList = doc.getElementsByTagName("train");

    for (int i = 0; i < nodeList.getLength(); i++) {
      Train train = new Train();
      //Chars chars;
      List<Carriage> carriages;
      //SpillMethod spillMethod;

      Node node = nodeList.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        Element element = (Element) node;

        train.setId(Integer.parseInt(element.getAttribute("trainId")));
        train.setName(element.getElementsByTagName("name").item(0).getTextContent());
        train.setType(element.getElementsByTagName("type").item(0).getTextContent());
        //train.setCarriageList(Boolean.parseBoolean(element.getElementsByTagName("al").item(0).getTextContent()));
        //train.setCarriageList(element.getElementsByTagName("manufacturer").item(0).getTextContent());

        carriages = getCarriages(element.getElementsByTagName("ingredients"));
        //chars = getChars(element.getElementsByTagName("chars"));
        //spillMethod = getSpillMethod(element.getElementsByTagName("spillMethod"));

        //chars.setSpillMethod(spillMethod);
        //beer.setChars(chars);
        train.setCarriageList(carriages);
        trains.add(train);
      }
    }
    return trains;
  }

  private List<Carriage> getCarriages(NodeList nodes) {
    List<Carriage> carriages = new ArrayList<>();
    if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE) {
      Element element = (Element) nodes.item(0);
      NodeList nodeList = element.getChildNodes();
      for (int i = 0; i < nodeList.getLength(); i++) {
        Node node = nodeList.item(i);
        if (node.getNodeType() == Node.ELEMENT_NODE) {
          Element el = (Element) node;
          carriages.add(new Carriage(el.getTextContent()));
        }
      }
    }

    return carriages;
  }

  /*  private Chars getChars(NodeList nodes){
        Chars chars = new Chars();
        if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE){
            Element element = (Element) nodes.item(0);
            chars.setAlcFraction(Double.parseDouble(element.getElementsByTagName("alcFraction").item(0).getTextContent()));
            chars.setFiltered(Boolean.parseBoolean(element.getElementsByTagName("filtered").item(0).getTextContent()));
            chars.setTransparency(Double.parseDouble(element.getElementsByTagName("transparency").item(0).getTextContent()));
            chars.setNutritions(Integer.parseInt(element.getElementsByTagName("nutritions").item(0).getTextContent()));
        }

        return chars;
    }
*/

  /*private SpillMethod getSpillMethod(NodeList nodes){
        SpillMethod spillMethod = new SpillMethod();
        if (nodes.item(0).getNodeType() == Node.ELEMENT_NODE){
            Element element = (Element) nodes.item(0);
            spillMethod.setVol(Double.parseDouble(element.getElementsByTagName("vol").item(0).getTextContent()));
            spillMethod.setMaterial(element.getElementsByTagName("tankMaterial").item(0).getTextContent());
        }
        return spillMethod;
    }*/
}
