package parser.dom;

import java.io.File;
import java.util.List;
import model.Train;
import org.w3c.dom.Document;

public class DOMParserUser {

  public static List<Train> getBeerList(File xml, File xsd) {
    DOMDocCreator creator = new DOMDocCreator(xml);
    Document doc = creator.getDocument();

//        try {
//            DOMValidator.validate(DOMValidator.createSchema(xsd),doc);
//        }catch (IOException | SAXException ex){
//            ex.printStackTrace();
//        }

    DOMDocReader reader = new DOMDocReader();

    return reader.readDoc(doc);
  }
}
