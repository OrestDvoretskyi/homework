package parser;


import comparator.TrainComparator;
import filechecker.ExtensionChecker;
import java.io.File;
import java.util.Collections;
import java.util.List;
import model.Train;
import parser.dom.DOMParserUser;
import parser.sax.SAXParserUser;
import parser.stax.StAXReader;

public class Parser {

  public static void main(String[] args) {
    File xml = new File("src\\main\\resources\\xml\\trainXML.xml");
    File xsd = new File("src\\main\\resources\\xml\\trainXSD.xsd");

    if (checkIfXML(xml) && checkIfXSD(xsd)) {
//            SAX
      printList(SAXParserUser.parseTrains(xml, xsd), "SAX");

//            StAX
      printList(StAXReader.parseTrains(xml, xsd), "StAX");

//            DOM
      printList(DOMParserUser.getBeerList(xml, xsd), "DOM");
    }
  }

  private static boolean checkIfXSD(File xsd) {
    return ExtensionChecker.isXSD(xsd);
  }

  private static boolean checkIfXML(File xml) {
    return ExtensionChecker.isXML(xml);
  }

  private static void printList(List<Train> trains, String parserName) {
    Collections.sort(trains, new TrainComparator());
    System.out.println(parserName);
    for (Train train : trains) {
      System.out.println(train);
    }
  }

}
