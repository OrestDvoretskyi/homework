package model;

/**
 * Created by Orest on 30/04/2018.
 */
public class Carriage {

  private int idNumber;
  private String type;
 // private boolean hasBar;
  //private List<Passenger>passengerList;


  public Carriage(int idNumber, String type) {
    this.idNumber = idNumber;
    this.type = type;
    //this.hasBar = hasBar;
  }

  public Carriage(String data) {
  }

  public int getIdNumber() {
    return idNumber;
  }

  public void setIdNumber(int idNumber) {
    this.idNumber = idNumber;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

 // public boolean isHasBar() {
 //   return hasBar;
 // }

 // public void setHasBar(boolean hasBar) {
   // this.hasBar = hasBar;
//  }

//  public List<Passenger> getPassengerList() {
  //  return passengerList;
 // }

  //public void setPassengerList(List<Passenger> passengerList) {
    //this.passengerList = passengerList;
  //}

  @Override
  public String toString() {
    return "Carriage{" +
        "idNumber=" + idNumber +
        ", type='" + type + '\'' +
       // ", hasBar=" + hasBar +
        //", passengerList=" + passengerList +
        '}';
  }
}
