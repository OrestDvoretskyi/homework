package model;

/**
 * Created by Orest on 30/04/2018.
 */
public class Passenger {

  private int id;
  private String passport;
  private String firstName;
  private String secondName;

  public Passenger() {
  }

  public Passenger(int id, String passport, String firstName, String secondName) {
    this.id = id;
    this.passport = passport;
    this.firstName = firstName;
    this.secondName = secondName;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getPassport() {
    return passport;
  }

  public void setPassport(String passport) {
    this.passport = passport;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getSecondName() {
    return secondName;
  }

  public void setSecondName(String secondName) {
    this.secondName = secondName;
  }

  @Override
  public String toString() {
    return "Passenger{" +
        "id=" + id +
        ", passport='" + passport + '\'' +
        ", firstName='" + firstName + '\'' +
        ", secondName='" + secondName + '\'' +
        '}';
  }
}
