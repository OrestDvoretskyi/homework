package model;

import java.util.List;

/**
 * Created by Orest on 30/04/2018.
 */
public class Train {

  private int id;
  private String type;
  private String name;
  private List<Carriage> carriageList;

  public Train() {
  }

  public Train(int id, String type, String name, List<Carriage> carriageList) {
    this.id = id;
    this.type = type;
    this.name = name;
    this.carriageList = carriageList;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<Carriage> getCarriageList() {
    return carriageList;
  }

  public void setCarriageList(List<Carriage> carriageList) {
    this.carriageList = carriageList;
  }

  @Override
  public String toString() {
    return "Train{" +
        "id=" + id +
        ", type='" + type + '\'' +
        ", name='" + name + '\'' +
        ", carriageList=" + carriageList +
        '}';
  }
}
