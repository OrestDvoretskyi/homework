package com.epam.dvoretsyi.homework3;

import java.io.Serializable;

/**
 * Created by Orest on 27/04/2018.
 */
public class Pirate implements Serializable{

  private int id;
  private transient int age;
  private String first_name;
  private  String  alias;

  public Pirate() {
  }

  public Pirate(int id, int age, String first_name, String alias) {
    this.id = id;
    this.age = age;
    this.first_name = first_name;
    this.alias = alias;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public String getFirst_name() {
    return first_name;
  }

  public void setFirst_name(String first_name) {
    this.first_name = first_name;
  }

  public String getAlias() {
    return alias;
  }

  public void setAlias(String alias) {
    this.alias = alias;
  }

  @Override
  public String toString() {
    return "Pirate{" +
        "id=" + id +
        ", age=" + age +
        ", first_name='" + first_name + '\'' +
        ", alias='" + alias + '\'' +
        '}';
  }
}
