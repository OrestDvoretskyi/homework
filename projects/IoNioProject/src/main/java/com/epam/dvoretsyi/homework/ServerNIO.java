package com.epam.dvoretsyi.homework;

/**
 * Created by Orest on 26/04/2018.
 */

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

public class ServerNIO {

  @SuppressWarnings("unused")
  public static void main(String[] args) throws IOException {

    // Selector: multiplexor of SelectableChannel objects
    Selector selector = Selector.open(); // selector is open here

    // ServerSocketChannel: selectable channel for stream-oriented listening sockets
    ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
    InetSocketAddress inetSocketAddress =
        new InetSocketAddress("localhost", 9999);

    // Binds the channel's socket to a local address and configures the socket to listen for connections
    serverSocketChannel.bind(inetSocketAddress);

    // Adjusts this channel's blocking mode.
    serverSocketChannel.configureBlocking(false);

    int ops = serverSocketChannel.validOps();
    SelectionKey selectKy = serverSocketChannel.register(selector, ops, null);

    // Infinite loop..
    // Keep server running
    while (true) {

      log("i'm a server and i'm waiting for new connection and buffer select...");
      // Selects a set of keys whose corresponding channels are ready for I/O operations
      selector.select();

      // token representing the registration of a SelectableChannel with a Selector
      Set<SelectionKey> keys = selector.selectedKeys();
      Iterator<SelectionKey> iterator = keys.iterator();

      while (iterator.hasNext()) {
        SelectionKey selectionKey = iterator.next();

        // Tests whether this key's channel is ready to accept a new socket connection
        if (selectionKey.isAcceptable()) {
          SocketChannel socketChannelClient = serverSocketChannel.accept();

          // Adjusts this channel's blocking mode to false
          socketChannelClient.configureBlocking(false);

          // Operation-set bit for read operations
          socketChannelClient.register(selector, SelectionKey.OP_READ);
          log("Connection Accepted: " +
              socketChannelClient.getLocalAddress() + "\n");

          // Tests whether this key's channel is ready for reading
        } else if (selectionKey.isReadable()) {

          SocketChannel socketChannelClient = (SocketChannel) selectionKey.channel();
          ByteBuffer byteBuffer = ByteBuffer.allocate(256);
          socketChannelClient.read(byteBuffer);
          String result = new String(byteBuffer.array()).trim();

          log("Message received: " + result);

          if (result.equals("Crunchify")) {
            socketChannelClient.close();
            log("\nIt's time to close connection as we got last company name 'Crunchify'");
            log("\nServer will keep running. Try running client again to establish new connection");
          }
        }
        iterator.remove();
      }
    }
  }

  private static void log(String str) {
    System.out.println(str);
  }
}
