package com.epam.dvoretsyi.homework2;

/**
 * Created by Orest on 27/04/2018.
 */

import java.io.BufferedReader;

import java.io.FileInputStream;

import java.io.IOException;

import java.io.InputStreamReader;

import java.util.ArrayList;

import java.util.List;

class Interval {

  private long start;
  private long end;

  public Interval(long start, long end) {

    this.start = start;
    this.end = end;
  }
}

class StatsRecord {

  private long   end;
  private long   duration;
  private int rate;

  public StatsRecord(long end, long duration, int rate) {

    this.end = end;
    this.duration = duration;
    this.rate = rate;
  }
}

public class ReadInputTest {


  List<StatsRecord> stats   = new ArrayList<>();

  List<Interval>    queries = new ArrayList<>();

  public static void main(String[] args) {

    ReadInputTest streams = new ReadInputTest();

    long startTime = System.currentTimeMillis();

    int nLines = 0, nQueries = 0;
    int size = 0;
    String input = null;
    String[] tmp = null;
    try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream("E:\\Soft\\input2.txt")))) {

      input = br.readLine();
      nLines = Integer.parseInt(input);

      if ((nLines >= 1) && (nLines <= 100_000)) {

        for (int i = 0; i < nLines; i++) {
          input = br.readLine();
          tmp = input.split(" ");
          StatsRecord sr = new StatsRecord(Long.parseLong(tmp[0]), Long.parseLong(tmp[1]), Integer.parseInt(tmp[2]));
          streams.stats.add(sr);
        }

      }

      input = br.readLine();
      nQueries = Integer.parseInt(input);

      if ((nQueries >= 1) && (nQueries <= 100_000)) {

        tmp = null;
        for (int i = 0; i < nQueries; i++) {
          input = br.readLine();
          tmp = input.split(" ");
          Interval in = new Interval(Long.parseLong(tmp[0]), Long.parseLong(tmp[1]));
          streams.queries.add(in);
        }

      }

      System.out.println("Streams: " + streams.stats.size());
      System.out.println("Queries: " + streams.queries.size());

      long endTime = System.currentTimeMillis();
      System.out.println("Execution Time: " + ((endTime - startTime)) + " milliseconds");
    }
    catch (IOException | NumberFormatException e) {
      System.out.println("Error processing the file!");
    }
  }
}


