package com.epam.dvoretsyi.homework;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;

/**
 * Created by Orest on 26/04/2018.
 */
public class ClientNIO {
  public static void main(String[] args) throws IOException, InterruptedException {

    InetSocketAddress crunchifyAddr =
        new InetSocketAddress("localhost", 9999);
    SocketChannel socketClientChannel = SocketChannel.open(crunchifyAddr);

    log("Connecting to Server on port 1111...");

    ArrayList<String> companyDetails = new ArrayList<String>();

    // create a ArrayList with companyName list
    companyDetails.add("Facebook");
    companyDetails.add("Twitter");
    companyDetails.add("IBM");
    companyDetails.add("Google");
    companyDetails.add("Crunchify");

    for (String companyName : companyDetails) {

      byte[] message = new String(companyName).getBytes();
      ByteBuffer buffer = ByteBuffer.wrap(message);
      socketClientChannel.write(buffer);




      log("sending: " + companyName);

      buffer.clear();

      // wait for 2 seconds before sending next message
      Thread.sleep(2000);
    }

    socketClientChannel.close();
  }

  private static void log(String str) {
    System.out.println(str);
  }
}
