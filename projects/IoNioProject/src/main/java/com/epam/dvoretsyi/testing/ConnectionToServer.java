package com.epam.dvoretsyi.testing;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;

/**
 * Created by Orest on 26/04/2018.
 */

//Here is how you open a DatagramChannel:
public class ConnectionToServer {

  public static void main(String[] args) throws IOException{

    DatagramChannel channel = DatagramChannel.open();
    channel.socket().bind(new InetSocketAddress(9999));

//Receiving Data
//You receive data from a DatagramChannel by calling its receive() method, like this:
    ByteBuffer buf = ByteBuffer.allocate(48);
    buf.clear();

    channel.receive(buf);

/*
The receive() method will copy the content of
 a received packet of data into the given Buffer. If
  the received packet contains more
data than the Buffer can contain, the remaining data is discarded silently.
 */
//Sending Data
//You can send data via a DatagramChannel by calling its send() method, like this:

    String newData = "New String to write to file..."
        + System.currentTimeMillis();

    buf = ByteBuffer.allocate(48);
    buf.clear();
    buf.put(newData.getBytes());
    buf.flip();

    int bytesSent = channel.send(buf, new InetSocketAddress("jenkov.com", 80));

    channel.connect(new InetSocketAddress("jenkov.com", 80));
    //When connected you can also use the read() and write() method,
    // as if you were using a traditional channel.
    // You just don't have any guarantees about delivery of the sent data.
    // Here are a few examples:

    int bytesRead = channel.read(buf);
    int bytesWritten = channel.write(buf);

    System.out.println(bytesRead);
    System.out.println(bytesWritten);
  }
}
