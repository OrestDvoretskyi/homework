package com.epam.dvoretsyi.homework3;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Orest on 27/04/2018.
 */
public class PiratesShip implements Serializable {


  public static void main(String[] args) throws IOException, ClassNotFoundException {

    List<Pirate> pirateList = new ArrayList<>();
    pirateList.add(new Pirate(1, 53, "Kiop", "Tuy"));
    pirateList.add(new Pirate(2, 23, "John", "Dog"));
    pirateList.add(new Pirate(3, 34, "Moter", "Shark"));
    FileOutputStream f = new FileOutputStream("E:\\Soft\\pirate.doc");
    ObjectOutputStream out = new ObjectOutputStream(f);
    out.writeObject(pirateList);
    out.flush();

    out.close();
    f.close();
    System.out.println("success");

    // deSerail.makeDeserial(

    DeserializableClass deSerail = new DeserializableClass();
    deSerail.makeDeserial();
    //}
  }


}