package com.epam.dvoretsyi.homework;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

/**
 * Created by Orest on 28/04/2018.
 */
public class ClientNIO3 {


    public static void main(String[] args) throws IOException, InterruptedException {

      InetSocketAddress crunchifyAddr =
          new InetSocketAddress("localhost", 9999);
      SocketChannel socketClientChannel = SocketChannel.open(crunchifyAddr);

      log("Connecting to Server on port 9999...");

      // ArrayList<String> companyDetails = new ArrayList<String>();
      BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
      System.out.println("If you want to send message press enter  1 ");
      String input = reader.readLine();

      //int number = Integer.parseInt(input);

      // create a ArrayList with companyName list
   /* companyDetails.add("Facebook");
    companyDetails.add("Twitter");
    companyDetails.add("IBM");
    companyDetails.add("Google");
    companyDetails.add("Crunchify");
*/
      // for (String companyName : companyDetails) {


      if(input=="1") {
        byte[] message = new String(input).getBytes();
        ByteBuffer buffer = ByteBuffer.wrap(message);
        socketClientChannel.read(buffer);

        socketClientChannel.write(buffer);
        log("sending: " + input);

        buffer.clear();

        // wait for 2 seconds before sending next message
        Thread.sleep(2000);
      }

      socketClientChannel.close();
    }

    private static void log(String str) {
      System.out.println(str);
    }
  }


