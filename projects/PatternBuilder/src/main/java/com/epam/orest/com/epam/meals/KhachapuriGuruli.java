package com.epam.orest.com.epam.meals;


import java.math.BigDecimal;

/**
 * Created by Orest on 14.04.2018.
 */
public class KhachapuriGuruli extends Khachapuri {

  @Override
  public String name() {
    return "KhachapuriGuruli";
  }

  @Override
  public double price() {
    return  25.0;
  }
}
