package com.epam.orest.com.epam.meals;

import com.epam.orest.com.epam.com.epam.builder.Item;
import com.epam.orest.com.epam.packing.Cup;
import com.epam.orest.com.epam.packing.Dish;


/**
 * Created by Orest on 14.04.2018.
 */
public abstract class Drink implements Item {

  @Override
  public Dish dish() {
    return new Cup();
  }

  @Override
  public abstract double price();

}