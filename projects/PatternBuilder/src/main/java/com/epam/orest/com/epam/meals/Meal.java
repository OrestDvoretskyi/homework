package com.epam.orest.com.epam.meals;

import com.epam.orest.com.epam.com.epam.builder.Item;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Orest on 14.04.2018.
 */
public class Meal {
  private List<Item> items = new ArrayList<Item>();
  public  void addItem(Item item){
    items.add(item);
  }

  public double getCost(){
    double cost =0.0;

    for (Item item:items){
      cost+=item.price();
    }
    return cost;
  }
  public  void showItems(){

    for (Item item:items){
      System.out.println("Item: " +item.name());
      System.out.println(", Put on: "+ item.dish().putOn());
      System.out.println(", Price: " + item.price());
    }
  }
}
