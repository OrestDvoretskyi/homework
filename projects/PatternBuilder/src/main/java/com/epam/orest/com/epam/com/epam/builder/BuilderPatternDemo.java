package com.epam.orest.com.epam.com.epam.builder;

import com.epam.orest.com.epam.meals.Meal;

/**
 * Created by Orest on 14.04.2018.
 */
public class BuilderPatternDemo {

  public static void main(String[] args) {
    MealBuilder mealBuilder = new MealBuilder();
    Meal vegMeal = mealBuilder.prepareVegMeal();
    System.out.println("\n\nFirst type of lunch:");
    vegMeal.showItems();
    System.out.println("Total Cost: " + vegMeal.getCost());

    Meal nonVegMeal = mealBuilder.prepareNonVegMeal();
    System.out.println("\n\nSecond type of lunch: ");
    nonVegMeal.showItems();
    System.out.println("Total Cost: " + nonVegMeal.getCost());

  }

}
