package com.epam.orest.com.epam.meals;

/**
 * Created by Orest on 14.04.2018.
 */

public class Chacha extends Drink {

  @Override
  public String name() {
    return "Chacha";
  }

  @Override
  public double price() {
    return 30.0;
  }
}
