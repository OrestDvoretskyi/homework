package com.epam.orest.com.epam.com.epam.builder;

import com.epam.orest.com.epam.meals.AjarianKhachapuri;
import com.epam.orest.com.epam.meals.Chacha;
import com.epam.orest.com.epam.meals.DryRedWine;
import com.epam.orest.com.epam.meals.KhachapuriGuruli;
import com.epam.orest.com.epam.meals.Meal;

/**
 * Created by Orest on 14.04.2018.
 */
public class MealBuilder {


  public Meal prepareVegMeal() {
    Meal meal = new Meal();
    meal.addItem(new KhachapuriGuruli());
    meal.addItem(new Chacha());
    return meal;
  }

  public Meal prepareNonVegMeal() {
    Meal meal = new Meal();
    meal.addItem(new AjarianKhachapuri());
    meal.addItem(new DryRedWine());
    return meal;

  }

}
