package com.epam.orest.com.epam.com.epam.builder;

import com.epam.orest.com.epam.packing.Dish;

/**
 * Created by Orest on 14.04.2018.
 */
public interface Item {

  public String name();

  public Dish dish();

  public double price();

}
