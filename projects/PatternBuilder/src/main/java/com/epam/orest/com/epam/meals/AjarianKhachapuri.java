package com.epam.orest.com.epam.meals;


import java.math.BigDecimal;

/**
 * Created by Orest on 14.04.2018.
 */
public class AjarianKhachapuri extends Khachapuri {

  @Override
  public String name() {
    return "Ajarian Khachapuri";
  }

  @Override
  public double price() {
    return 50.0;
  }
}
