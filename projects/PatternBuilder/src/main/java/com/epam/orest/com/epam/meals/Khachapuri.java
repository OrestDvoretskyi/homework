package com.epam.orest.com.epam.meals;

import com.epam.orest.com.epam.com.epam.builder.Item;
import com.epam.orest.com.epam.packing.Dish;
import com.epam.orest.com.epam.packing.Plate;

/**
 * Created by Orest on 14.04.2018.
 */
public abstract class Khachapuri implements Item {


  @Override
  public Dish dish() {
    return new Plate();
  }

  @Override
  public abstract double price();
}
