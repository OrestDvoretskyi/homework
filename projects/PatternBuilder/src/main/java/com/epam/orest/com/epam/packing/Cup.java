package com.epam.orest.com.epam.packing;

/**
 * Created by Orest on 14.04.2018.
 */
public class Cup implements Dish {

  @Override
  public String putOn() {
    return "Cup";
  }
}
