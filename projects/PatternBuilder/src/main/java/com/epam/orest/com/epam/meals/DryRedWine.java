package com.epam.orest.com.epam.meals;

import java.math.BigDecimal;

/**
 * Created by Orest on 14.04.2018.
 */
public class DryRedWine extends Drink {

  @Override
  public String name() {
    return "DryRedWine";
  }

  @Override
  public double price() {
    return  35.0;
  }
}
