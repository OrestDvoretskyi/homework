package com.epam.dvoretskyi;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Created by Orest on 24/04/2018.
 */
public class StringUtils {


  public static void main(String[] args) {
    int id;
    String first_name="John";
    String second_name="Haryson";
    String middle_name="Godest";
    String city="Furst";
    String hello = new String("Hello!");
    String fullName = first_name.concat(second_name).concat(middle_name);
    String fullInformation = first_name.concat(second_name).concat(middle_name).concat(city);
    StringUtils stringUtils = new StringUtils();
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append(fullInformation);

    System.out.println(  fullInformation=stringBuilder.toString());

    //############

    String language = new String("de");
    String country = new String("Deutsch");

    Locale locale = new Locale(language, country);
    Locale bLocale = new Locale.Builder()
        .setLanguage(language).setRegion(country).build();
    Locale cLocale = Locale.GERMANY;


    //############
    ResourceBundle messages = ResourceBundle
        .getBundle("messages", locale);
    System.out.println(messages.getString("main.welcome"));
    System.out.println(messages.getString("main.description"));



    //messages.properties
    //messages_en_US.properties

    // main.welcome=Hello world
   //  main.description=It's English version




  }

  /*

  String s1 = new String("Hello");
	String s2 = "And Goodbye";
	String str = s1 + s2;
	str = s1.concat(s2);

  StringBuilder sb =
	new StringBuilder(s1);
sb.append(s2);
str = sb.toString( );


StringBuilder sb =
	new StringBuilder(s1);
sb.append(s2);
str = sb.toString( );


StringBuffer sa =
	new StringBuffer( );
sa.append(s1);
sa.append(s2);
String str =  sa.toString( );


   */


}
