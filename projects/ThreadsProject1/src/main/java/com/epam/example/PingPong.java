package com.epam.example;

/**
 * Created by Orest on 02/05/2018.
 */

//Create ping pong/ with wait and notify
public class PingPong extends Thread  {

  public void run (){
    synchronized ("abc"){
     try{"abc".wait();
       System.out.println("thread has been notified");
     }catch (Exception e){e.printStackTrace();}
    }
  }

}

