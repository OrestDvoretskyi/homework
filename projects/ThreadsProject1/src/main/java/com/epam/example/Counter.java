package com.epam.example;

/**
 * Created by Orest on 03/05/2018.
 */
public class Counter {
  long count = 0;

  public synchronized void add(long value){
    this.count += value;
  }
}
