package com.epam.example.callable;

import java.util.Random;
import java.util.concurrent.Callable;

/**
 * Created by Orest on 04/05/2018.
 */
public class CallableExample implements Callable{
  public Object call() throws Exception
  {
    Random generator = new Random();
    Integer randomNumber = generator.nextInt(5);

    Thread.sleep(randomNumber * 1000);

    return randomNumber;
  }
}
