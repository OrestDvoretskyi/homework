package com.epam.example;

/**
 * Created by Orest on 03/05/2018.
 */
public class ExampleSynchronized extends Thread{
  protected Counter counter = null;

  public ExampleSynchronized(Counter counter){
    this.counter = counter;
  }

  public void run() {
    for(int i=0; i<10; i++){
      counter.add(i);
      System.out.println("Throw");
    }
  }
}
