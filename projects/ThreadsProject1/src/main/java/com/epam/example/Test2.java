package com.epam.example;

/**
 * Created by Orest on 03/05/2018.
 */
public class Test2 extends Thread {

  public static void main(String[] args) throws InterruptedException {
    Test2 test2 = new Test2();
    test2.start();
    Thread.sleep(500);

    // главный поток входит в монитор t
    // (= this в строке /*1*/)
    synchronized (test2) {
      // и выполняет вызов notify на мониторе
      // которым владеет главный поток
      test2.notify();
    }
  }



  public void run() {
    // монитором является this /*1*/
    synchronized (this) {
      try {
        // перевод в режим ожидания:
        wait();
        // вывод сообщения при
        // пробуждении:
        System.out.println(
            "thread has been notified");
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

}