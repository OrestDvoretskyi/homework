package com.epam.example;

/**
 * Created by Orest on 03/05/2018.
 */
public class ExampleExecute {

  public static void main(String[] args) {
    Counter counter = new Counter();
    Thread  threadA = new ExampleSynchronized(counter);
    Thread  threadB = new ExampleSynchronized(counter);

    threadA.start();
    threadB.start();
  }
}
