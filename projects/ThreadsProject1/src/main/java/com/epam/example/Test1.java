package com.epam.example;

/**
 * Created by Orest on 03/05/2018.
 */
public class Test1 extends Thread {
  public synchronized void m() {
    while (true) System.out.println(
        Thread.currentThread().getName());
  }

  public Test1() {this.start();}
  public void run() {this.m();}

  public static void main(String[] argv) {
    Test1 treadA = new Test1(); // запуск потока A
   Test1 threadB = new Test1(); // запуск потока B
  }
  // в процессе выполнения  будут выводиться
  // имена потоков threadA и threadB вперемешку

}
