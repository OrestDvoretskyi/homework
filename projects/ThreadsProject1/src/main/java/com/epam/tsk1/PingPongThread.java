package com.epam.tsk1;

/**
 * Created by Orest on 03/05/2018.
 */
public class PingPongThread extends Thread {

  private String Message;

  public PingPongThread(String Message) {
    this.Message = Message;
  }

  public static void main(String[] argv)
      throws Exception {
    new PingPongThread("Catch :First rocket : Throw").start();
    new PingPongThread("Catch :Second rocket : Throw").start();


  }

  @Override
  public void run() {

    super.run();

    synchronized (PingPongThread.class) {
      try {
        while (true) {

          PingPongThread.class.notify();
          PingPongThread.class.wait();
          System.out.println(this.Message);
        }
      } catch (Exception e) {
        e.printStackTrace();
      }
    }

  }
}