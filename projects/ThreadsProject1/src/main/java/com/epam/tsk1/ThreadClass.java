package com.epam.tsk1;

/**
 * Created by Orest on 02/05/2018.
 */
public class ThreadClass {
//Write simple “ping-pong” program using wait() and notify().

  public static class PingThread1 extends Thread {

    @Override
    public void run() {
      super.run();
      System.out.println( Thread.currentThread().getName()+"Catch");
      //System.out.println("Catch");
    }
  }

  public static void main(String[] args) {
    // System.out.println( Thread.currentThread().getName());

    Thread pingThread1 = new PingThread1();
    Thread pingThread2 = new PingThread2();
    for (pingThread1.isAlive();pingThread2.isAlive();) {
      pingThread1.start();
      pingThread2.start();
    }


  }

  public static class PingThread2 extends Thread {

    @Override
    public void run() {
      super.run();

      System.out.println( Thread.currentThread().getName()+"Throw");
    }
  }
}
