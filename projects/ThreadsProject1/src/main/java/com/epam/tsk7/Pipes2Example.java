package com.epam.tsk7;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

/**
 * Created by Orest on 03/05/2018.
 */
public class Pipes2Example {
  public static void main(String[] args) throws Exception {
    PipedInputStream pis = new PipedInputStream();
    PipedOutputStream pos = new PipedOutputStream();
    pos.connect(pis);

    Runnable producer = () -> produceData(pos);
    Runnable consumer = () -> consumeData(pis);
    new Thread(producer).start();
    new Thread(consumer).start();
  }

  public static void produceData(PipedOutputStream pos) {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    String input = null;
    try {
      input = reader.readLine();
    } catch (IOException e) {
      e.printStackTrace();
    }
    int number = Integer.parseInt(input);
    try {
      for (number = 1; number <= 50; number++) {
        pos.write((byte) number);
        pos.flush();
        System.out.println("Writing: " + number);
        Thread.sleep(500);
      }
      pos.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
  public static void consumeData(PipedInputStream pis) {
    try {
      int num = -1;
      while ((num = pis.read()) != -1) {
        System.out.println("Reading: " + num);
      }
      pis.close();
    } catch (Exception e) {
      e.printStackTrace();
    }

  }
}
