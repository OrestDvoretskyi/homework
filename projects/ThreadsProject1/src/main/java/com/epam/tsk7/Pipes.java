package com.epam.tsk7;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;

/**
 * Created by Orest on 03/05/2018.
 */
public class Pipes {

  public static void main(String[] args) throws IOException {

    final PipedOutputStream output = new PipedOutputStream();
    final PipedInputStream input = new PipedInputStream(output);

    Thread thread1 = new Thread(new Runnable() {
      @Override
      public void run() {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String input = null;
        try {
          input = reader.readLine();
        } catch (IOException e) {
          e.printStackTrace();
        }

        int number = Integer.parseInt(input);
        try {
          System.out.println(number);
          output.write("Hello world, pipe!".getBytes());

        } catch (IOException e) {
        }

      }
    });

    Thread thread2 = new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          int data = input.read();
          while (data != -1) {
            System.out.print((char) data);
            data = input.read();
          }
        } catch (IOException e) {
        }
      }
    });

    thread1.start();
    thread2.start();

  }
}
