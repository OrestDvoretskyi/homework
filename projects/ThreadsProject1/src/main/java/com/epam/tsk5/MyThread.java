package com.epam.tsk5;

import java.util.Date;

/**
 * Created by Orest on 06/05/2018.
 */
public class MyThread implements Runnable{

  private String command;

  public MyThread(String s){
    this.command=s;
  }

  @Override
  public void run() {
    System.out.println(Thread.currentThread().getName()+" Start. Time = "+new Date());
    processCommand();
    System.out.println(Thread.currentThread().getName()+" End. Time = "+new Date());
  }

  private void processCommand() {
    try {
      Thread.sleep(5000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  @Override
  public String toString(){
    return this.command;
  }
}
