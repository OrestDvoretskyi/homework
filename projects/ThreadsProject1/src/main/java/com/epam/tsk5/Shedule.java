package com.epam.tsk5;

import java.util.Date;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by Orest on 06/05/2018.
 */
public class Shedule {

/*
Create a task that sleeps for a random amount
 of time between 1 and 10 seconds, then displays its sleep time and exits.
 Create and run a quantity (given on the command line) of these tasks.
 Do it by using ScheduledThreadPool.

 */

  public static void main(String[] args) throws InterruptedException {


    ScheduledExecutorService scheduledThreadPool = Executors.newScheduledThreadPool(5);


    Random random = new Random();


    //schedule to run after sometime
    //random.ints().limit(10).sorted().forEach(System.out::println);
    System.out.println("Current Time = " + new Date());
    for (int i = 0; i < 10; i++) {

      Thread.sleep(1000);

      MyThread myThread = new MyThread("do heavy processing");

      scheduledThreadPool.schedule(myThread, 10, TimeUnit.SECONDS);
    }

    //add some delay to let some threads spawn by scheduler
    Thread.sleep(3000);

    scheduledThreadPool.shutdown();
    while (!scheduledThreadPool.isTerminated()) {
      //wait for all tasks to finish
    }
    System.out.println("Finished all threads");
  }
}
