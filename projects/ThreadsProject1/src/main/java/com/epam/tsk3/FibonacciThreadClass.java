package com.epam.tsk3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Orest on 03/05/2018.
 */

/*
Create a task that produces a sequence of n Fibonacci numbers,
where n is provided to the constructor of the task.
Create a number of these tasks and drive them using threads.

 */
public class FibonacciThreadClass  {

  public FibonacciThreadClass(String firstTestTask) {
  }

  public static void main(String[] args) throws InterruptedException {


    Thread fibbthread = new Thread(new FibbThread(), "FibbThread");

    fibbthread.start();
//FibbThread fibbThread1 = new FibbThread();

  }


  public static class FibbThread implements Runnable {


  /*  public static int fib(int arg) throws InterruptedException {
      // System.out.println("" + arg);
      if (arg < 2) {
        return 1;

      } else {
        Thread.sleep(500);

        return fib(arg - 2) + fib(arg - 1);

      }

    }*/
  public  static int fibNonRec(int arg) throws InterruptedException {
      /*BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

      String input = null;
      try {
        input = reader.readLine();
      } catch (IOException e) {
        e.printStackTrace();
      }
      int number1 = Integer.parseInt(input);*/
    if (arg<2){
      return 1;
    }
    int[]result = new int[arg+1];
    result[0]=1;
    result[1]=1;
    for (int k=2;k<result.length;k++){
      Thread.sleep(500);
      result[k]=result[k-2]+result[k-1];
    }return result[arg];
  }


    @Override
    public void run() {
      System.out.println( Thread.currentThread().getName());
      BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

      String input = null;
      try {
        input = reader.readLine();
      } catch (IOException e) {
        e.printStackTrace();
      }
      int number = Integer.parseInt(input);

      try {

        for (int i = 0; i < number; i++) {
          System.out.println("fib (" + i + ")" + fibNonRec(i));

        }

      } catch (InterruptedException e) {
        e.printStackTrace();
      }

    }
  }


}
