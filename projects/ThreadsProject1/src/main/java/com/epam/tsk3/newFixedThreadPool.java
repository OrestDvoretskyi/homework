package com.epam.tsk3;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Orest on 03/05/2018.
 */
public class newFixedThreadPool {
  public static void main(String[] args) {
    ExecutorService execService = Executors.newFixedThreadPool(2);
    execService.execute(new TestTask("FirstTestTask"));
    execService.execute(new TestTask("SecondTestTask"));
    execService.execute(new TestTask("ThirdTestTask"));

    execService.shutdown();
  }
}
