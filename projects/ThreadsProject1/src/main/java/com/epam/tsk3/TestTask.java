package com.epam.tsk3;

/**
 * Created by Orest on 03/05/2018.
 */

import org.apache.log4j.Logger;

public class TestTask implements Runnable{

  private static Logger log = Logger.getLogger(TestTask.class);
  private String taskName;

  public TestTask(String taskName) {
    this.taskName = taskName;
  }

  public void run() {
    try {
      log.debug(this.taskName + " is sleeping...");
      Thread.sleep(3000);
      log.debug(this.taskName + " is running...");
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}

