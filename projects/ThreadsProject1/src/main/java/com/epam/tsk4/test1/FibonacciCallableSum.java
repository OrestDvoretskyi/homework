package com.epam.tsk4.test1;


import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

/**
 * Created by Orest on 04/05/2018.
 */
public class FibonacciCallableSum {

  public static void main(String[] args) throws InterruptedException {

    FutureTask[] randomNumberTasks = new FutureTask[1];

    for (int i = 0; i < 1 ; i++) {
      Callable callable = new MyCallableFibb();

      // Create the FutureTask with Callable
      randomNumberTasks[i] = new FutureTask(callable);

      // As it implements Runnable, create Thread
      // with FutureTask
      Thread t = new Thread(randomNumberTasks[i]);
      t.start();
    }

/*    for (int i = 0; i < 5; i++)
    {
      // As it implements Future, we can call get()
      System.out.println(randomNumberTasks[i].get());

      // This method blocks till the result is obtained
      // The get method can throw checked exceptions
      // like when it is interrupted. This is the reason
      // for adding the throws clause to main
    }*/
    //}
  }
}






