package com.epam.tsk4;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Created by Orest on 04/05/2018.
 */
public class Test implements Callable<Integer> {

  int number;

  public Test(int i) {
  }

  @Override
  public Integer call() throws Exception {
   // fib(number);
    /*int arg = 0;
    if (arg < 2) {
      return 1;
    }
    int[] result = new int[arg + 1];
    result[0] = 1;
    result[1] = 1;
    for (int k = 2; k < result.length; k++) {
      Thread.sleep(500);
      result[k] = result[k - 2] + result[k - 1];
    }
    return result[arg];*/
    return fib(number);
  }
//  }

  static int fib(int arg)
  {
    int a = 0, b = 1, c;
    if (arg == 0)
      return a;
    for (int i = 2; i <= arg; i++)
    {
      c = a + b;
      a = b;
      b = c;
    }
    return b;
  }

  /*@Test(expected = ExecutionException.class)
  public void whenTaskSubmitted_ThenFutureResultObtained() {
    Test test = new Test(5);
    Future<Integer> future = executorService.submit(test);

    assertEquals(120, future.get().intValue());
  }*/
  public static void main(String[] args) {

  }
}
