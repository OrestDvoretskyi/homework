package com.epam.tsk4.test1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.channels.Pipe.SourceChannel;
import java.util.Random;
import java.util.concurrent.Callable;

/**
 * Created by Orest on 04/05/2018.
 */
public class MyCallableFibb implements Callable {



  public Object call() throws Exception {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    String input = reader.readLine();

    int number = Integer.parseInt(input);
    System.out.println(fibNonRec(number));
    //Random generator = new Random();
    //Integer randomNumber = generator.nextInt(5);
    int calculateSum= fibNonRec( number+ 2) - 1;

    Thread.sleep(fibNonRec(number) +calculateSum * 1000);

    return fibNonRec(number)+calculateSum;
  }

  public static int fibNonRec(int arg) throws InterruptedException {
      BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

      String input = null;
      try {
        input = reader.readLine();
      } catch (IOException e) {
        e.printStackTrace();
      }
      int number = Integer.parseInt(input);
    if (number< 2) {
      return 1;
    }
    int[] result = new int[number + 1];
    result[0] = 1;
    result[1] = 1;
    for (int k = 2; k < result.length; k++) {
      Thread.sleep(500);
      result[k] = result[k - 2] + result[k - 1];
    }
    return result[arg];
  }



}
