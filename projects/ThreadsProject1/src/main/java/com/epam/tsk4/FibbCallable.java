package com.epam.tsk4;

import com.epam.example.callable.MyCallable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Created by Orest on 04/05/2018.
 */
public class FibbCallable implements Callable<String> {

  public static int fibNonRec(int arg) throws InterruptedException {
      /*BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

      String input = null;
      try {
        input = reader.readLine();
      } catch (IOException e) {
        e.printStackTrace();
      }
      int number1 = Integer.parseInt(input);*/
    if (arg < 2) {
      return 1;
    }
    int[] result = new int[arg + 1];
    result[0] = 1;
    result[1] = 1;
    for (int k = 2; k < result.length; k++) {
      Thread.sleep(500);
      result[k] = result[k - 2] + result[k - 1];
    }
    return result[arg];
  }
  public String call() throws InterruptedException {
    Thread.sleep(1000);
    // возвращает имя потока, который выполняет callable таск
    return Thread.currentThread().getName();
  }

  public static void main(String args[]){
    //Получаем ExecutorService утилитного класса Executors с размером gпула потоков равному 10
    ExecutorService executor = Executors.newFixedThreadPool(10);
    //создаем список с Future, которые ассоциированы с Callable
    List<Future<String>> list = new ArrayList<Future<String>>();
    // создаем экземпляр MyCallable
    Callable<String> callable = new MyCallable();
    for(int i=0; i< 100; i++){
      //сабмитим Callable таски, которые будут
      //выполнены пулом потоков
      Future<String> future = executor.submit(callable);
      //добавляя Future в список,
      //мы сможем получить результат выполнения
      list.add(future);
    }

    for(Future<String> fut : list){

      try {
        // печатаем в консоль возвращенное значение Future
        // будет задержка в 1 секунду, потому что Future.get()
        // ждет пока таск закончит выполнение
        System.out.println(new Date()+ "::" + fut.get());
      } catch (InterruptedException | ExecutionException e) {
        e.printStackTrace();
      }
    }

    executor.shutdown();
  }


}
