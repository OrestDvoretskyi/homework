package com.epam.tsk4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.Callable;

/**
 * Created by Orest on 04/05/2018.
 */
public class NEstt  implements Callable{
  public static int fibNonRec(int arg) throws InterruptedException {

    int fibSum=0;
    if (arg < 2) {
      return 1;
    }
    int[] result = new int[arg + 1];
    result[0] = 1;
    result[1] = 1;
    for (int k = 2; k < result.length; k++) {
      Thread.sleep(500);
      result[k] = result[k - 2] + result[k - 1]+result[k+k];
      //fibSum =result[k]+result[k];
      //System.out.println(fibSum);

    }

       return result[(arg+2)-1];

  }
  static int calculateSum(int arg) throws InterruptedException {
    return fibNonRec( arg+ 2) - 1;
  }


  @Override
  public Object call() throws InterruptedException {
    System.out.println(Thread.currentThread().getName());
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    String input = null;
    try {
      input = reader.readLine();
    } catch (IOException e) {
      e.printStackTrace();
    }
    int number = Integer.parseInt(input);

    try {
      int fibSum=0;
      // fibSum= fibNonRec(number)+fibNonRec(number);
      for (number = 0; number < 5; number++) {
        System.out.println("fib (" + number + ")" + fibNonRec(number));


      }

    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    return fibNonRec(number);
  }
}

