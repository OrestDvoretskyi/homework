package com.epam.tsk4;

import com.epam.tsk4.test1.MyCallableFibb;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

/**
 * Created by Orest on 04/05/2018.
 */
public class CallableFutureTest {
  public static void main(String[] args) throws Exception
  {

    //newWorkStealingPool

    // FutureTask is a concrete class that
    // implements both Runnable and Future
    FutureTask[] fibfutureTasks = new FutureTask[3];
//create fibonacci algorithm

    for (int i = 0; i < 3; i++) {
      //System.out.println("fib (" + number + ")" + fibNonRec(number));

      Callable callable = new MyCallableFibb();

      // Create the FutureTask with Callable
      fibfutureTasks[i] = new FutureTask(callable);

      // As it implements Runnable, create Thread
      // with FutureTask
      Thread t = new Thread(fibfutureTasks[i]);
      t.start();

    }
    for (int i = 0; i < 5; i++)
    {
      // As it implements Future, we can call get()
      System.out.println(fibfutureTasks[i].get());

      // This method blocks till the result is obtained
      // The get method can throw checked exceptions
      // like when it is interrupted. This is the reason
      // for adding the throws clause to main
    }
  }
}
