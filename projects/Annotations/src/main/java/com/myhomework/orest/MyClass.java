package com.myhomework.orest;


/**
 * Created by Orest on 17.04.2018.
 */

public class MyClass {

  @MyCustomAnnotation(id = 1, studentName = "Helga"
      , stuEmail = "123@gmail.com")
  private int id;
  private int age;
  private String firstName;
  private String secondName;
  private String email;


  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getSecondName() {
    return secondName;
  }

  public void setSecondName(String secondName) {
    this.secondName = secondName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public MyClass(int id, int age, String firstName, String secondName, String email) {
    this.id = id;
    this.age = age;
    this.firstName = firstName;
    this.secondName = secondName;
    this.email = email;
  }

  public MyClass() {
  }

  @Override
  public String toString() {
    return "MyClass{" +
        "id=" + id +
        ", age=" + age +
        ", firstName='" + firstName + '\'' +
        ", secondName='" + secondName + '\'' +
        ", email='" + email + '\'' +
        '}';
  }


}
