package com.myhomework.orest;


import com.epam.orest.MyTestAnnotation.Priority;

/**
 * Created by Orest on 18/04/2018.
 */


public class TestAnnotationClass {

  @AnnotationTest
  void testA(){
    if(true)
      throw  new RuntimeException("This test always failed");
  }
  @AnnotationTest(enabled=false)
  void  testB(){
    if (false)
      throw  new RuntimeException("This test always passed");
  }
  @AnnotationTest(enabled =true)
  void  testC(){
    if(10>1){
      //do nothing, this test always passed.
    }
  }
}
