package com.myhomework.orest;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Orest on 17.04.2018.
 */

@Retention(RetentionPolicy.RUNTIME)

@Target(ElementType.METHOD) //can use in method only.
public @interface AnnotationTest {

  public boolean enabled() default true;

}
