package com.myhomework.orest;

import com.epam.orest.MyTest;
import com.epam.orest.MyTestAnnotation;
import com.epam.orest.TestExample;
import com.myhomework.orest.MyCustomAnnotation.Priority;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
//import java.lang.reflect.Method;

/**
 * Created by Orest on 17.04.2018.
 */
public class RunAnnotationTesting {
void test(){
  System.out.println("1. -testing annotation");
  Class refClass=MyClass.class;
  Field[] fields =
      refClass.getDeclaredFields();
  refClass.getName();
  for (Field field:fields){
    if (field.isAnnotationPresent(MyCustomAnnotation.class)){
      MyCustomAnnotation myCustomAnnotation =
          (MyCustomAnnotation)field.getAnnotation(MyCustomAnnotation.class);
          String priority = String.valueOf(Priority.HIGH);
          int age = myCustomAnnotation.studentAge();
          String name = myCustomAnnotation.studentName();
      System.out.println("field: "+field.getName());
      System.out.println(age);
      System.out.println(priority);


    }
  }
}





}
























 /* public static void main(String[] args) {
    System.out.println("Testing...");

    int passed = 0, failed = 0, count = 0, ignore = 0;

    Class<TestExample> obj = TestExample.class;

    // Process @TesterInfo
    if (obj.isAnnotationPresent(MyTestAnnotation.class)) {

      Annotation annotation = obj.getAnnotation(MyTestAnnotation.class);
      MyTestAnnotation myTestAnnotation = (MyTestAnnotation) annotation;

      System.out.printf("%nPriority :%s", myTestAnnotation.priority());
      System.out.printf("%nCreatedBy :%s", myTestAnnotation.createdBy());
      System.out.printf("%nTags :");

      int tagLength = myTestAnnotation.tags().length;
      for (String tag : myTestAnnotation.tags()) {
        if (tagLength > 1) {
          System.out.print(tag + ", ");
        } else {
          System.out.print(tag);
        }
        tagLength--;
      }

      System.out.printf("%nLastModified :%s%n%n", myTestAnnotation.lastModified());

    }

    // Process @Test
    for (Method method : obj.getDeclaredMethods()) {

      // if method is annotated with @Test
      if (method.isAnnotationPresent(MyTest.class)) {

        Annotation annotation = method.getAnnotation(MyTest.class);
        MyTest test = (MyTest) annotation;

        // if enabled = true (default)
        if (test.enabled()) {

          try {
            method.invoke(obj.newInstance());
            System.out.printf("%s - Test '%s' - passed %n", ++count, method.getName());
            passed++;
          } catch (Throwable ex) {
            System.out.printf("%s - Test '%s' - failed: %s %n", ++count, method.getName(), ex.getCause());
            failed++;
          }

        } else {
          System.out.printf("%s - Test '%s' - ignored%n", ++count, method.getName());
          ignore++;
        }

      }

    }
    System.out.printf("%nResult : Total : %d, Passed: %d, Failed %d, Ignore %d%n", count, passed, failed, ignore);
  }*/

  /*
   private void test1() {
    System.out.println("1 - Test field with annotation");
    Class clazz = MyClassA.class;
    Field[] fields = clazz.getDeclaredFields();
    for (Field field : fields) {
      // if field is annotated with @MyField
      if (field.isAnnotationPresent(MyField.class)) {
        MyField myField = (MyField) field.getAnnotation(MyField.class);
        String name = myField.name();
        int age = myField.age();
        System.out.println("field: " + field.getName());
        System.out.println("  name in @MyField: " + name);
      }
    }
  }

  private void test2() {
    System.out.println("2 - Test unknown object");
    MyClassA myClassA = new MyClassA();
    reflUnknownObject(myClassA);
  }
   */


