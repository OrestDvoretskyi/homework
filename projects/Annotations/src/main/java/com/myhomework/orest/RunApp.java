package com.myhomework.orest;

/**
 * Created by Orest on 18/04/2018.
 */
public class RunApp {

  public static void main(String[] args) {
GenericClass<MyClass> gen = new GenericClass<>(MyClass.class);
    MyClass a =new MyClass();
    Class aclass = a.getClass();
    System.out.println(aclass);
    MyClass i = new MyClass();
    int iclass = i.getId();
    System.out.println(iclass);
    MyClass str =new MyClass();

    RunAnnotationTesting runAnnotationTesting
        = new RunAnnotationTesting();
    runAnnotationTesting.test();

    System.out.println();
  }
}
