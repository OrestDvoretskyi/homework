package com.myhomework.orest;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Orest on 17.04.2018.
 */
@Documented
@Target(ElementType.FIELD)
@Inherited
@Retention(RetentionPolicy.RUNTIME)
public @interface MyCustomAnnotation {
  public enum Priority {
    LOW, MEDIUM, HIGH}

  Priority priority() default Priority.MEDIUM;

  int id();
  int studentAge()default 18;
  String studentName();
  String stuEmail();
  String stuClass() default "JavaDev";


}
