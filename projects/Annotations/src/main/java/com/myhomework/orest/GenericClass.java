package com.myhomework.orest;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

/**
 * Created by Orest on 18/04/2018.
 */
public class GenericClass<T> {


  private T obj;
  // Compile Error!
//  public MyGenericClass() {
//    this.obj = new T();
//  }
  private final Class<T> myClass;

  public GenericClass(Class<T> myClass) {
    this.myClass = myClass;
    try {
      obj = myClass.newInstance();

      // 1. Class.newInstance() can only invoke the zero-argument constructor,
      // while Constructor.newInstance() may invoke any constructor, regardless of the number of parameters.

      // 2. Class.newInstance() requires that the constructor be visible;
      // Constructor.newInstance() may invoke private constructors under certain circumstances.
      obj = myClass.getConstructor().newInstance();

      System.out.println("The declared fields of class " + myClass.getSimpleName() + " are : ");
      Field[] fields = myClass.getDeclaredFields();
      // Printing field names
      for (Field field : fields) {
        System.out.println("  " + field.getName());
      }
      System.out.println();

    } catch (InstantiationException e) {
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      e.printStackTrace();
    } catch (InvocationTargetException e) {
      e.printStackTrace();
    } catch (NoSuchMethodException e) {
      e.printStackTrace();
    }
  }
}

