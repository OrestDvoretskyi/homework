package com.com.epam.lviv;

import java.sql.SQLException;

/**
 * Created by Orest on 20/04/2018.
 */
public class JdbcClass {

  private static final String JDBC_URL ="jdbc:mysql://localhost:3306/sample?serverTimezone=UTC&useSSL=false";
  private static final String user = "root";
  private static final String password = "golovach";

  public static void main(String[] args)throws SQLException {
    com.mysql.jdbc.Driver driver= new com.mysql.jdbc.Driver();

    System.out.println(driver.acceptsURL("jdbc:my_data_base"));
    System.out.println(driver.acceptsURL(JDBC_URL));
    try (java.sql.Connection conn = driver.connect(JDBC_URL,)){}
  }

  private static void readData() {
  }
/*while (rs.next())
  {
    String city = rs.getString("City");
    System.out.format("%s\n", city);
  }

      rs.close();
statement.close();
connection.close();

  Statement stmt =
      con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
          ResultSet.CONCUR_READ_ONLY);
  String query = "select students from class where type=‘not sleeping";
  ResultSet rs = stmt.executeQuery( query );
rs.previous(); // go back in the RS (not possible in JDBC 1…)
rs.relative(-5); // go 5 records back
rs.relative(7); // go 7 records forward
rs.absolute(100); // go to 100th record
  public static void main(String[] args) {


    String city = "Lviv";
    String citynew = "Sambir";
    PreparedStatement preparedStatement;


    preparedStatement=connection.prepareStatement
        ("UPDATE city SET City=? WHERE City=?;");
    preparedStatement.setString(1, citynew);
    preparedStatement.setString(2, city);
    int n=preparedStatement.executeUpdate();
  }
  */

}
