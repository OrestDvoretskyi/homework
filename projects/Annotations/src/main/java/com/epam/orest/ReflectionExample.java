package com.epam.orest;

import java.lang.reflect.Modifier;

/**
 * Created by Orest on 16.04.2018.
 */

public class ReflectionExample {

  private int id;
  private int age;
  private String firstName;
  private String secondName;
  private String email;


  public static void main(String[] args) {
    //Initial stuff
    ReflectionExample reflectionExample = new ReflectionExample();
    Class cls = reflectionExample.getClass();
    //Get class  name
    cls.getName();
    cls.getSimpleName();

    //Example modifiers
    Modifier.isPrivate(cls.getModifiers());
    Modifier.isAbstract(cls.getModifiers());

    //Get parent and package

    cls.getSuperclass().getName();
    cls.getPackage();

    Class stringClass = java.lang.String.class;
    Class<Integer> integerClass = int.class;
    Class<Integer> integerClass1 = Integer.TYPE;
    Class aClass = integerClass.getClass();

    try {
      Class aClass1 = Class.forName("java.lang.String");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
  }
}

