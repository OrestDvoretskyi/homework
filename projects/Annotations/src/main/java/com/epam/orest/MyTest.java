package com.epam.orest;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Orest on 16.04.2018.
 */
@Deprecated
@Retention(RetentionPolicy.RUNTIME)

@Target(ElementType.METHOD) //can use in method only.

public @interface MyTest {


  public boolean enabled() default true;


}
