package com.epam.orest;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import org.hibernate.annotations.Table;

/**
 * Created by Orest on 16.04.2018.
 */

@Entity
@Table(appliesTo = "person")

public class AnnotationExamples {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "person_id",
      nullable = false, length = 25)
  private int g;
@Column(name= "surname", nullable = false,length = 25)

private String  surname ;

@Column(name = "name",nullable =  false, length = 25)
private String name;

@Column(name = "email",nullable = true,length = 45)
private String email;

@ManyToOne
@JoinColumn(name = "city_id",referencedColumnName = "city_id")
//private  City city;
  public void rest() {
  }


}
