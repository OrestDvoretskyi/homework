package com.epam.orest;

;

import com.epam.orest.MyTestAnnotation.Priority;


/**
 * Created by Orest on 16.04.2018.
 */

@MyTestAnnotation(
    priority = Priority.HIGH,
    createdBy = "Orest",
    tags = {"sales","test"}
)
public class TestExample {
  @MyTest
  void testA(){
    if(true)
      throw  new RuntimeException("This test always failed");
  }
  @MyTest(enabled=false)
  void  testB(){
    if (false)
      throw  new RuntimeException("This test always passed");
  }
  @MyTest(enabled =true)
  void  testC(){
    if(10>1){
      //do nothing, this test always passed.
    }
  }


}

