package com.epam.orest;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Orest on 16.04.2018.
 */
@Deprecated
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface MyTestAnnotation {

  public enum Priority {
    LOW, MEDIUM, HIGH
  }


  Priority priority() default Priority.MEDIUM;

  String[] tags() default "";

  String createdBy() default "Orest";

  String lastModified() default "03/01/2018";

}
