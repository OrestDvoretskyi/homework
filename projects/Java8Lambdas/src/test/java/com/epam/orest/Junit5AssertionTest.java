package com.epam.orest;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.Test;

/**
 * Created by Orest on 15.04.2018.
 */
public class Junit5AssertionTest {
  @Test
  public void testAssert(){

  //Variable declaration


  String string1="Junit";
  String string2="Junit";
  String string3="test";
  String string4="test";
  String string5=null;
  int variable1=1;
  int	variable2=2;
  int[] airethematicArrary1 = { 1, 2, 3 };
  int[] airethematicArrary2 = { 1, 2, 3 };

  //Assert statements
  assertEquals(string1,string2);
  assertSame(string3, string4);
  assertNotSame(string1, string3);
  assertNotNull(string1);
  assertNull(string5);
  assertTrue(variable1<variable2);
  assertArrayEquals(airethematicArrary1, airethematicArrary2);
}

}

