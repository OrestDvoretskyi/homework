package lviv.epam;

/**
 * Created by Orest on 12.04.2018.
 */
public class Student {

  private int id;
  private String name;
  private String surname;
  private String city;
  private  int age;
  private double success;

  public Student(int id, String name, String surname, String city, int age,double success) {
    this.id = id;
    this.name = name;
    this.surname = surname;
    this.city = city;
    this.age = age;
    this.success =success;
  }

  public Student() {
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  public double getSuccess() {
    return success;
  }

  public void setSuccess(double success) {
    this.success = success;
  }

  @Override
  public String toString() {
    return "Student{" +
        "id=" + id +
        ", name='" + name + '\'' +
        ", surname='" + surname + '\'' +
        ", city='" + city + '\'' +
        ", age=" + age + '\'' +
        ",success= "+ success+
        '}';
  }

}
