package lviv.epam;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Created by Orest on 12.04.2018.
 */

/*
Create a few methods that returns list (or array) of random integers.
 Methods should use streams API and should be
 implemented using different Streams generators.
Count average, min, max, sum of list values.
Try to count sum using both reduce and sum Stream methods
Count number of values that are bigger than average

 */
public class StreamApi {

  public static void main(String[] args) {
    List<Student> studentList = new ArrayList();
    studentList.add(new Student
        (1, "Oleg", "Golovenko", "Lviv", 23, 67.6));
    studentList.add(new Student
        (2, "Petr", "Xerx", "Uzgorod", 25, 78.6));
    studentList.add(new Student
        (3, "Nazar", "Artemenko", "Ternopil", 28, 82.8));
    //Stream.of(1,2,4,3,5).max(Comparator.naturalOrder())
    //  .ifPresent(maxInt->System.out.println("Maximum number in the set is " + maxInt));

    //get sum
    double sum = studentList.stream().filter(o -> o.getSuccess() > 10.0).mapToDouble(o -> o.getSuccess()).sum();
    System.out.println("sum of success:" + sum);
    //find min
    Student student1 = studentList.stream().min(Comparator.comparingInt(Student::getAge)).get();
    System.out.println("User with minimum age:" + student1);

    //find max
    Student student2 = studentList.stream().max(Comparator.comparingInt(Student::getAge)).get();
    System.out.println("User with maximum age:" + student2);

    //find count of students
    System.out.println("Count of students: " + studentList.stream().count());

    //stream reduce
    Student st1 = studentList.stream().reduce(Compare::min).get();
    System.out.println("Minimum age of student (using reduce) is: " + st1);
    Student st2 = studentList.stream().reduce(Compare::max).get();
    System.out.println("Maximum age of student (using reduce) is: " + st2);

   /* IntStream.of(studentList.getAge().mapToDouble(i->i+i/3.0)
        .forEach(i-> System.out.format("%.2f",i));

        IntStream.of(().mapToDouble(i->i+i/3.0)
        .forEach(i-> System.out.format("%.2f",i));
    //find average


   // studentList= Stream.of(studentList) .mapToInt(s -> s.st) .average();
*/
    ;

 //   studentList=stream().collect( groupingBy (i -> i/5*5,counting()));
//Map<Integer, List<Integer>> map = Stream.of(2, 34, 54, 23, 33, 20, 59, 11, 19, 37) .collect( groupingBy (i -> i/10 * 10 ) );
//Map<Integer, Long> map = Stream.of(2, 34, 54, 23, 33, 20, 59, 11, 19, 37) .collect( groupingBy(i -> i/10 * 10, counting() ) );

  }
}
