package ua.epam.lviv.com.task2;

/**
 * Created by Orest on 13.04.2018.
 */
public class Product {
  private String name = "Laptop";
  private int quantity = 10;

  public void reserve(){
    System.out.println("Stock [ Name: "+name+
        ",Quantity: " + quantity +" ] bought");
  }
  public void buy(){
    System.out.println("Stock [ Name: "+name+
        ",Quantity: " + quantity +" ] sold");
  }

}
