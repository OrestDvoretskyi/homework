package ua.epam.lviv.com.task2;

/**
 * Created by Orest on 13.04.2018.
 */
public class BuyProduct implements Purchase{
    private Product product;

    public BuyProduct(Product product){
      this.product = product;
    }

     @Override
  public void execute() {
    product.buy();
  }
}

