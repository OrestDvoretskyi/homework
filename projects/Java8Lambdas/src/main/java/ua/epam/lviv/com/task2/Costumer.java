package ua.epam.lviv.com.task2;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Orest on 13.04.2018.
 */
public class Costumer {

    private List<Purchase> purchaseList = new ArrayList<Purchase>();

    public void takePurchase(Purchase purchase){
      purchaseList.add(purchase);
    }

    public void placeProduct(){

      for (Purchase purchase : purchaseList) {
        purchase.execute();
      }
      purchaseList.clear();
    }
  }

