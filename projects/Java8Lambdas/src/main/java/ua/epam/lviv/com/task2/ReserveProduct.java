package ua.epam.lviv.com.task2;

/**
 * Created by Orest on 13.04.2018.
 */
public class ReserveProduct implements Purchase {

    private Product product;

    public ReserveProduct(Product product){
      this.product = product;
    }



    @Override
    public void execute() {
      product.reserve();
    }
  }




