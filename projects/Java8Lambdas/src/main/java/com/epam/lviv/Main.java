package com.epam.lviv;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;

/**
 * Created by Orest on 12.04.2018.
 */

/*
1....Create functional interface with method
that accepts three int values and return int value.
 Create lambda functions (as variables in main method)
what implements this interface:
First lambda returns max value
Second – average
Invoke thous lambdas.




2....Implement pattern Command. Each command has its name (with which it is invoked) and one string argument. You should implement 4 commands with next ways: command as lambda function, as method reference, as anonymous class, as object of command class. User enters command name and argument into console, your app invokes corresponding command.

 */

public class Main {

  public static void main(String[] args) {
    //max_value

    myFunctional myFunctional1 = (a, b, c) -> a > b ? a > c ? a : c : b > c ? b : c;
    System.out.println(myFunctional1.add(1, 4, 6));
    //avg
    myFunctional myFunctional12 = (a, b, c) -> (a + b + c) / 3;

    System.out.println(myFunctional12.add(4, 6, 7));
  }


  myFunctional myFunctional = new myFunctional() {
    @Override
    public double add(double a, double b, double c) {

      return 0;
    }
  };



/*
Stream.of(1,2,4,3,5).max(Comparator.comparing(i -> i))
.ifPresent(maxInt->System.out.println("Maximum number in the set is " + maxInt));
does the same as

Stream.of(1,2,4,3,5).max(Comparator.naturalOrder())
.ifPresent(maxInt->System.out.println("Maximum number in the set is " + maxInt));
 */


}

