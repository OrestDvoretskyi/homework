package com.epam.lab;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * Created by Orest on 27.03.2018.
 */
public class Application {

  private static Logger logger = LogManager.getLogger(Application.class);

  public static void main(String[] args) {
    logger.trace("This is a trace massage");
    logger.debug("This is a debug massage");
    logger.info("This is a info massage");
    logger.warn("This is a warm massage");
    logger.error("This is a error massage");
    logger.fatal("This is a fatal massage");
  }
}
